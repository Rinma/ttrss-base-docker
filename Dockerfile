FROM php:7.3-buster

LABEL maintainer="me@marvin-dalheimer.de"

RUN apt-get update && \
    apt-get install -y vim curl g++ zlib1g-dev libicu-dev libpq-dev libpng-dev libcurl4-openssl-dev libxml2-dev \
        postgresql postgresql-server-dev-all postgresql-contrib && \
    apt-get autoclean && \
    apt-get purge && \
    rm -rf /var/cache/apt

RUN docker-php-ext-configure intl
RUN docker-php-ext-install pdo pdo_pgsql pgsql intl json mbstring gd xml opcache curl
